package neto.jsfmaventest.controller;

import java.util.Date;

import javax.enterprise.inject.Model;

@Model
public class PrimeController {

    private Date aDate;

    public Date getaDate() {
        return aDate;
    }

    public void setaDate(Date aDate) {
        this.aDate = aDate;
    }

}
